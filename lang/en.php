<?
//install
$lang_install["Installation"] = "Insalace";
$lang_install["MySQL-hostname"] = "MySQL - hostname";
$lang_install["MySQL-username"] = "MySQL - username";
$lang_install["MySQL-password"] = "MySQL - password";
$lang_install["MySQL-database"] = "MySQL - database";
$lang_install["Submit"] = "Submit";
$lang_install["Game-name"] = "Název hry";
$lang_install["Game-description"] = "Popis hry";
//default page
$lang_default["register"] = "Register";
$lang_default["login"] = "Login";
$lang_default["no-news"] = "Nothing new here";
//game it self
//$lang_game[]
$lang_page['select_order'] = "Order";
$lang_page['by_alphabet'] ="Alphabet ascent";
$lang_page['by_alphabet_des'] ="Alphabet descent";
$lang_page['by_date'] ="Date ascent";
$lang_page['by_date_des'] ="Date descent";
$lang_page['folders'] = "Folders";
?>
