<?
//install
$lang_install["Installation"] = "Insalace";
$lang_install["MySQL-hostname"] = "MySQL - hostname";
$lang_install["MySQL-username"] = "MySQL - username";
$lang_install["MySQL-password"] = "MySQL - password";
$lang_install["MySQL-database"] = "MySQL - database";
$lang_install["Submit"] = "Predložit";
$lang_install["Game-name"] = "Název hry";
$lang_install["Game-description"] = "Popis hry";
//default page
$lang_default["register"] = "Registrace";
$lang_default["login"] = "Přihlásení";
$lang_default["no-news"] = "Nejsou žádne novinky";
//game it self
//$lang_game[]
$lang_page['select_order'] = "Řazení";
$lang_page['by_alphabet'] ="Podle abecedy vzestupně";
$lang_page['by_alphabet_des'] ="Podle abecedy sestupně";
$lang_page['by_date'] ="Podle data vzestupně";
$lang_page['by_date_des'] ="Podle data sestupně";
$lang_page['folders'] = "Složky";
?>
