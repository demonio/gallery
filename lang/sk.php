<?
//install
$lang_install["Installation"] = "Inštalácia";
$lang_install["MySQL-hostname"] = "MySQL - hostname";
$lang_install["MySQL-username"] = "MySQL - username";
$lang_install["MySQL-password"] = "MySQL - password";
$lang_install["MySQL-database"] = "MySQL - database";
$lang_install["Submit"] = "Predložiť";
$lang_install["Game-name"] = "Názov hry";
$lang_install["Game-description"] = "Popis hry";
//default page
$lang_default["register"] = "Registrácia";
$lang_default["login"] = "Prihlásenie";
$lang_default["no-news"] = "Niesú tu žiadne novinky";
//game it self
//$lang_game[]
$lang_game["buildings"] = "Budovy";
$lang_game["research"] = "Výzkumy";
$lang_game["units"] = "Jednotky";
$lang_game["map"] = "Mapa";
$lang_game["shop"] = "Obchod";
$lang_game["logout"] = "Odhlásiť sa";
$lang_game["alliance"] = "Alliancia";
$lang_game["settings"] = "Nastavenia";
$lang_game["upgrade"] = "Vylepšenia";
$lang_game["downgrade"] = "Degradovať";
$lang_game["cost"] = "Cena";
$lang_game["produce"] = "Produkcia";
$lang_game["require"] = "Vyžaduje";
$lang_game["build"] = "Vytvoriť";
$lang_game["attack"] = "Útok";
$lang_game["defense"] = "Brániť";
$lang_game["life"] = "Životy";
$lang_game["speed"] = "Rýchlost";
$lang_game["cargo"] = "Náklad";
$lang_game["no_resources"] = "Nedostatok zdrojov pre túto operáciu";
$lang_game["upgrade_time"] = "Vylepšenie bude hotové za ";
$lang_game["seconds"] = " Sekundy";
$lang_game["upgrade_in_progress"] = "Vylepšenie už prebieha";
$lang_game["time_rem"] = "Akcia bude dokončená o:";
?>
