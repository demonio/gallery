<?

function order(){
	global $lang_page;
	$select_order = $lang_page['select_order'];
	$a = $lang_page['by_alphabet'];
	$b = $lang_page['by_alphabet_des'];
	$c = $lang_page['by_date'];
	$d = $lang_page['by_date_des'];
	$e = get_parameters("order");
	echo <<< EOL
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">$select_order <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<li><a href="?order=1$e">$a</a></li>
			<li><a href="?order=2$e">$b</a></li>
			<li><a href="?order=3$e">$c</a></li>
			<li><a href="?order=4$e">$d</a></li>
		</ul>
	</li>
EOL;
}

function search_input(){
	echo <<< EOL
	<form class="navbar-form navbar-left" id="search" action="javascript:search();">
	    <input type="text" id="hledej" name="search" class="form-control col-lg-8" placeholder="Search">
	</form>
EOL;
}

function folders_drop(){
	global $lang_page;
	global $DB;
	echo "<li class=\"dropdown\">";
	echo "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">".$lang_page['folders']." <span class=\"caret\"></span></a>";
	echo "<ul class=\"dropdown-menu\">";
	$result = sql_folders_drop();
	for($i = 0, $c = count($result); $i < $c; $i++)
		echo "<li><a href='?folder=".$result[$i]['id'].get_parameters("folder")."'>".$result[$i]['jmeno']."</a></li>";
	echo "</ul>";
	echo "</li>";
}

function display_folder($id, $name, $description, $thubnail, $time){
	$link = get_parameters("folder");
	if ($thubnail == 0) {
		$thubnail = "no-image.png";
	}
	echo <<< EOL
	<a href="?folder=$id$link" class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <h3 class="panel-title">$name</h3>
	        </div>
	        <div class="panel-body">
	            <div class="row-picture">
	                <img class="circle" src="images/$thubnail">
	            </div>
	            <div class="row-content">
	                <p class="list-group-item-text">
	                    $description
	                </p>
	            </div>
	        </div>
	        <div class="panel-footer">
	            $time
	        </div>
	    </div>
	</a>
EOL;
}

function compress_image($source_url, $destination_url, $quality) {
	$info = getimagesize($source_url);

	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

	//save file
	imagejpeg($image, $destination_url, $quality);
}

function clean_name($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function display_image($id, $folder, $format, $name, $description, $time, $display_id){
	$link = get_parameters("folder");
	$image_link = "images/".$folder."/".clean_name($name).".".$format;
	echo <<< EOL
	<a href="#" class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
	    <div class="panel panel-success">
	        <div class="panel-body">
	                <img onclick="toggle($display_id)" id="img$display_id" class="obrazek" src="$image_link">
	        </div>
	    </div>
	</a>
EOL;
}

function get_latest_images(){
	echo <<< EOL
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	        <ol class="carousel-indicators">
	          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	          <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
	          <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
	        </ol>
	        <div class="carousel-inner" role="listbox">
	          <div class="item next left">
	            <img data-src="holder.js/1140x500/auto/#777:#555/text:First slide" alt="First slide [1140x500]" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTE0MCIgaGVpZ2h0PSI1MDAiIHZpZXdCb3g9IjAgMCAxMTQwIDUwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvMTE0MHg1MDAvYXV0by8jNzc3OiM1NTUvdGV4dDpGaXJzdCBzbGlkZQpDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48IVtDREFUQVsjaG9sZGVyXzE1MmZlNTJmN2IxIHRleHQgeyBmaWxsOiM1NTU7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6NTdwdCB9IF1dPjwvc3R5bGU+PC9kZWZzPjxnIGlkPSJob2xkZXJfMTUyZmU1MmY3YjEiPjxyZWN0IHdpZHRoPSIxMTQwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzc3NyIvPjxnPjx0ZXh0IHg9IjM5MC41MDc4MTI1IiB5PSIyNzUuNSI+Rmlyc3Qgc2xpZGU8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true">
	          </div>
	          <div class="item">
	            <img data-src="holder.js/1140x500/auto/#666:#444/text:Second slide" alt="Second slide [1140x500]" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTE0MCIgaGVpZ2h0PSI1MDAiIHZpZXdCb3g9IjAgMCAxMTQwIDUwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvMTE0MHg1MDAvYXV0by8jNjY2OiM0NDQvdGV4dDpTZWNvbmQgc2xpZGUKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTJmZTUzMDBkNCB0ZXh0IHsgZmlsbDojNDQ0O2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjU3cHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MmZlNTMwMGQ0Ij48cmVjdCB3aWR0aD0iMTE0MCIgaGVpZ2h0PSI1MDAiIGZpbGw9IiM2NjYiLz48Zz48dGV4dCB4PSIzMzUuNjAxNTYyNSIgeT0iMjc1LjUiPlNlY29uZCBzbGlkZTwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
	          </div>
	          <div class="item active left">
	            <img data-src="holder.js/1140x500/auto/#555:#333/text:Third slide" alt="Third slide [1140x500]" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTE0MCIgaGVpZ2h0PSI1MDAiIHZpZXdCb3g9IjAgMCAxMTQwIDUwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvMTE0MHg1MDAvYXV0by8jNTU1OiMzMzMvdGV4dDpUaGlyZCBzbGlkZQpDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48IVtDREFUQVsjaG9sZGVyXzE1MmZlNTMwYmRhIHRleHQgeyBmaWxsOiMzMzM7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6NTdwdCB9IF1dPjwvc3R5bGU+PC9kZWZzPjxnIGlkPSJob2xkZXJfMTUyZmU1MzBiZGEiPjxyZWN0IHdpZHRoPSIxMTQwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzU1NSIvPjxnPjx0ZXh0IHg9IjM3Ny44NjcxODc1IiB5PSIyNzUuNSI+VGhpcmQgc2xpZGU8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true">
	          </div>
	        </div>
	        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	          <span class="sr-only">Previous</span>
	        </a>
	        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	          <span class="sr-only">Next</span>
	        </a>
	</div>
EOL;
}

function get_folders($folder){
	$result = sql_get_folders($folder);
	for($i = 0, $c = count($result); $i < $c; $i++)
		display_folder($result[$i]["id"], $result[$i]["jmeno"], $result[$i]["popisek"], $result[$i]["nahled"], $result[$i]["cas"]);
}

function get_images($folder){
	$result = sql_get_images($folder);
	for($i = 0, $c = count($result); $i < $c; $i++)
		display_image($result[$i]["id"], $result[$i]["slozka"], $result[$i]["format"], $result[$i]["jmeno"], $result[$i]["popisek"], $result[$i]["cas"], $i);
}

function get_parameters($except){
	$string = "";
	foreach ($_GET as $key => $value)  {
		if ($key != $except) {
			$string .= "&$key=$value";
		}
	}
	return $string;
}
?>
