<?php
session_start();
require_once("mysql.php");
require_once("global-var.php");
require_once("../lang/".$language.".php");
require_once("functions.php");
if(!isset($_SESSION['user'])) {
	echo "
<div class='alert alert-dismissible alert-danger'>
  <button type='button' class='close' data-dismiss='alert'>×</button>
  <strong>Oh snap!</strong> You are no longer signed in please refresh page.
</div>
	";
}
include("../themes/".$theme."/nav.php");
include("../themes/".$theme."/settings.php");
?>
