<nav class="navbar navbar-inverse fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="javascript:history.go(0)"><?php echo $gamename; ?></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li><a href="javascript:redirect('buildings')"><?php echo $lang_game["buildings"]; ?></a></li>
        <li><a href="javascript:redirect('research')"><?php echo $lang_game["research"]; ?></a></li>
        <li><a href="javascript:redirect('units')"><?php echo $lang_game["units"]; ?></a></li>
        <li><a href="javascript:redirect('map')"><?php echo $lang_game["map"]; ?></a></li>
        <!-- Comming in 0.4
        <li><a href="javascript:redirect('shop')"><?php echo $lang_game["shop"]; ?></a></li>
        <li><a href="javascript:redirect('alliance')"><?php echo $lang_game["alliance"]; ?></a></li>-->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="javascript:redirect('settings')"><?php echo $lang_game["settings"]; ?></a></li>
        <li><a href="javascript:logout()"><?php echo $lang_game["logout"]; ?></a></li>
      </ul>
    </div>
  </div>
</nav>
