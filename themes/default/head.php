<!-- You can put link to your custom css here -->
<!-- Your site -->
  <!-- nav bar -->
  <div class="navbar navbar-inverse">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>
<a href="?" class="navbar-brand" href="javascript:void(0)"><?php echo $title ?></a>
</div>
<div class="navbar-collapse collapse navbar-inverse-collapse">
    <?php search_input() ?>
    <ul class="nav navbar-nav">
        <?php order(); ?>
        <?php folders_drop(); ?>
    </ul>
<ul class="nav navbar-nav navbar-right">
        <!-- TODO -->
</ul>
</div>
</div>
