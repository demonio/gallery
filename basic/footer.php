<div class="signature" >
<p>
Version 1.0.2</b>
<br>
Created by <a target="_blank" href="https://www.linkedin.com/profile/view?id=412196427&trk=hp-identity-name">Václav Müller</a>
source at <a target="_blank" href="https://bitbucket.org/demonio/tbg-engine">BitBucket</a>
created under <a target="_blank" href="https://creativecommons.org/licenses/by-nd/4.0/legalcode"><img src="data/licence/88x31.png" ></a> license.

</p>
</div>
</body>
</html>
