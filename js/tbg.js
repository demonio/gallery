$(document).on('click', '.modal-backdrop', function (event) {
    bootbox.hideAll()
});
var id = 0;
//TODO pridat order do nastaveni
var order_value = 0;
function order(value){
    order_value = value;
    refresh(id);
}
function refresh(value){
	    id = value;
	    ajax_control = jQuery.ajax({
	    url: "refresh-look.php",
	    type: "POST",
	    data: {folder: id, order: order_value}
	    });
	    ajax_control.always(function(){
	    $('#content').html(ajax_control.responseText);
	    });
}
function folder(){
	    $("#folder").ajaxSubmit({url: 'create-folder.php', type: 'post', clearForm: true, resetForm: true, success: refresh(id)});
	    setTimeout(function(){
		bootbox.hideAll();
	    	refresh(id);
	    }, 2000);
	    return false;
}
function file(){
	    $("#data").ajaxSubmit({url: 'upload-file.php', type: 'post', clearForm: true, resetForm: true, success: refresh(id) });
	    setTimeout(function(){
		bootbox.hideAll();
	    	refresh(id);
	    }, 2000);
	    return false;
}
function search(){
		 var data_string = document.getElementById('hledej').value;
	    ajax_control = jQuery.ajax({
	    url: "search.php",
	    type: "POST",
	    data: {hledej: data_string}
	    });
	    ajax_control.always(function(){
	    $('#content').html(ajax_control.responseText);
	    });
	    return false;
}
var cislo = 0;
function nacist(){
	refresh(id);
}

function toggle(cislo2){
cislo = cislo2;
if(!document.getElementById("img"+cislo)){
	bootbox.hideAll();
}
var img = document.getElementById("img"+cislo).src;
//TODO opravit aby se to zavrelo
bootbox.dialog({
  title: "Gallery",
  message: '<a href="'+img+'"><img class="nastred-obr" src="'+img+'"/></a>',
  buttons: {
 prev: {
      label: "Předchozí­",
      className: "btn btn-danger",
      callback: function() {
	bootbox.hideAll();
        cislo = cislo - 1;
	toggle(cislo);
      }
    },
    next: {
      label: "Následující­",
      className: "btn btn-success",
      callback: function() {
	bootbox.hideAll();
        cislo = cislo + 1;
	toggle(cislo);
      }
    }
  }
});
}

function zmena() {
	$("div.panel.panel-success").toggleClass("slozka-ramecek");
}
$(document).on('click', '.modal-backdrop', function (event) {
    bootbox.hideAll()
});
document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;
    if (e.keyCode == '37') {
       // left arrow
	bootbox.hideAll();
	cislo--;
	toggle(cislo);
    }
    else if (e.keyCode == '39') {
       // right arrow
	bootbox.hideAll();
	cislo++;
	toggle(cislo);
    }
    else if (e.keyCode == '27') {
	bootbox.hideAll();
	cislo = 0;
    }

}

function logout() {
	$.ajax({url: "php/logout.php",
		success: function(data)
           {
               $('#content').html(data);
           }});
}
function redirect(where) {
	$.ajax({url: "php/"+where+".php",
		success: function(data)
           {
               $('#content').html(data);
           }});
}
