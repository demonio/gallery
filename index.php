<?php
session_start();
mb_internal_encoding("UTF-8");
mb_http_output( "UTF-8" );
ob_start("mb_output_handler");
require_once("config/config.php");
require_once("config/$sql_engine.php");
require_once("php/global-var.php");
require_once("lang/".$language.".php");
require_once("php/$sql_engine.php");
require_once("php/functions.php");

include("basic/head.php");
if(file_exists("themes/".$theme."/head.php"))
    include("themes/".$theme."/head.php");
include("basic/body.php");
if(isset($_SESSION["user"]) ) {

} elseif (isset($_GET["page"])) {
    include("themes/".$theme."/".$_GET["page"].".php");
}else {
	include("themes/".$theme."/default.php");
}
include("basic/js.php");
if(file_exists("themes/".$theme."/js.php"))
    include("themes/".$theme."/js.php");
include("basic/footer.php");
?>
